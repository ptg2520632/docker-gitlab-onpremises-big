# Line notifications

1. To get access key

visit this [link](https://developers.line.biz/en/docs/messaging-api/getting-started/#using-console) official from line to registration and get the key and can read future more in [the documentation](https://notify-bot.line.me/doc/en/)

**Note for understand**

> There have 2 ways to trigger each job that failed/success or not by **when:** and **after_script:**

But for this demonstration will use **when** because no need to write if-else statements in bash script for check success/failure but when is can trigger for us

## when:

1. Define cleanup stage

```yaml
stages:
  - ...
  - clean_up
```

1. Adding `clean_up_job` job

Adding before the last job (or adding after the stage that you want to cleanup (trigger))

This notify job will be notified when the job is failed from the eailer stage

`on_failure:` Run the job only when at least one job in an earlier stage fails. A job in an earlier stage with allow_failure: true is always considered successful.

So, Executes `clean_up_job` only when `job` fails.

```yaml
clean_up_job:
  stage: clean_up
  image: curlimages/curl:latest
  when: on_failure # trigger only when earlier stage has failed at least one job
  script:
    - echo "test"
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[💥 Failed stage:$CI_JOB_STAGE - stage:$CI_JOB_NAME] Pipeline $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) was failed."'
```

1. Adding `notify-pipeline` job to the notify stage naming it notify job

```yaml
notify-pipeline:
  stage: notify
  image: curlimages/curl:latest
  script:
    - echo "Line notification"
  when: always # always triggered
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[🚀 Pipeline finished]   $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) has finished."'
```

You can look [Predefined](https://docs.gitlab.com/16.2/ee/ci/variables/predefined_variables.html) to use in your pipeline (.gitlab-ci.yml)
And look [when: condition](https://docs.gitlab.com/ee/ci/yaml/#when)

So, final `.gitlab-ci.yml` will look like

```yaml
# Define Stages for ordering in script
stages:
  - build
  - test
  - docker-build
  - deploy
  - clean_up
  - notify

default:
  tags:
    - shared

# This folder is cached between builds
# https://docs.gitlab.com/ee/ci/yaml/index.html#cache
# cache:
#   paths:
#     - node_modules/

build:
  stage: build
  # Official framework image. Look for the different tagged releases at:
  # https://hub.docker.com/r/library/node/tags/
  image: node
  script:
    - echo "Start building App"
    - npm install
    - npm run build
    - echo "Build successfully!"
  # artifacts:
  #   expire_in: 1 hour
  #   paths:
  #     - build
  #     - node_modules/

test:
  stage: test
  image: node
  script:
    - npm install
    - echo "Testing App-"
    - CI=true npm run lint
    - echo "Test successfully!"

docker-build:
  stage: docker-build
  image: docker:latest
  services:
    # - name: docker:24.0.3-dind
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout $CI_REGISTRY_ADDRESS
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker build -t "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE" . #CI_REGISTRY_IMAGE can look at docker hub of our repository
    # - docker scout quickview
    - docker push "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"

deploy:
  stage: deploy
  image: docker
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout $CI_REGISTRY_ADDRESS
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker pull "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
    - docker stop $APP_NAME || true && docker rm $APP_NAME || true
    - docker run -p 4000:80 -d --name $APP_NAME "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"

clean_up_job:
  stage: clean_up
  image: curlimages/curl:latest
  when: on_failure # trigger only when earlier stage has failed at least one job
  script:
    - echo "test"
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[💥 Failed stage:$CI_JOB_STAGE - stage:$CI_JOB_NAME] Pipeline $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) was failed."'

notify-pipeline:
  stage: notify
  image: curlimages/curl:latest
  script:
    - echo "Line notification"
  when: always # always triggered
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[🚀 Pipeline finished]   $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) has finished."'
```
