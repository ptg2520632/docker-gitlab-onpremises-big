# SETUP GITLAB RUNNER

## Overview

1. Install gitlab-runner
   [GitLab-Runner:](https://docs.gitlab.com/runner/install/index.html)
2. verify gitlab-runner
   [GitLab-Runner-Linux:](https://docs.gitlab.com/runner/install/linux-repository.html)
3. Register
   `gitlab-runner register`
   [GitLab-Runner](https://docs.gitlab.com/runner/)

## Let's start

1. Update System Packages:

```bash
   sudo apt update # ubuntu

   sudo dnf update # rocky
```

2. Install GitLab Runner:

**Case linux/unix:**

### Ubuntu

- Download and install the GitLab Runner package:

```bash
  sudo curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

- Install the GitLab Runner package:

```bash
  sudo apt-get install gitlab-runner
```

- Grant permission to user (no need to using sudo)

```bash
   sudo usermod -aG gitlab-runner ${USER}
```

will add sudo permissions to current User

To apply the new group membership, log out of the server and back in

```bash
   su - ${USER}
```

### Rocky

- Update the package manager:

```bash
   sudo dnf update
```

- Install dependencies

```bash
   sudo dnf install -y curl policycoreutils openssh-server perl
```

- Install GitLab Runner:

```bash
   curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
   sudo dnf install -y gitlab-runner_amd64.rpm
```

- Set up and start GitLab Runner service:

```bash
   sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
   sudo gitlab-runner start
```

**Case Windows:**

1.  Create a folder somewhere in your system, ex.: `C:\GitLab-Runner`
2.  Download the binary for `64-bit` or `32-bit` and put it into the folder you created.
3.  Make sure to restrict the Write permissions on the GitLab Runner directory and executable.

    To restrict the write permissions on the GitLab Runner directory and executable in Windows, you can follow these steps:

    1. Locate the folder where you have placed the GitLab Runner binary (e.g., `C:\GitLab-Runner`).

    2. Right-click on the folder and select `"Properties."`

    3. In the Properties window, go to the `"Security"` tab.

    4. Click on the `"Edit"` button to change the permissions.

    5. In the Permissions window, you'll see a list of user groups and their permissions for the folder. By default, the `"Users"` group may have `"Full control"` or `"Modify"` permissions.

    6. To restrict write permissions, select the `"Users"` group, and then click on the `"Edit"` button.

    7. In the `"Allow"` column, uncheck the `"Write"` permission. You can also uncheck `"Modify"` to further restrict access.

    8. Click "OK" to apply the changes.

    9. Run an elevated command prompt: (Run as administrator)

Then **Run an elevated command prompt: (running as administrator)**

Run service using Built-in System Account (under directory created in step 1. from above, ex.: `C:\GitLab-Runner`)

```bash
cd C:\GitLab-Runner
.\gitlab-runner.exe install
.\gitlab-runner.exe start
```

3. Register

```bash
   sudo gitlab-runner register
   sudo gitlab-runner start
```

4. Verify GitLab Runner Connection:

**Linix:**

```bash
   sudo gitlab-runner status
   sudo nano /etc/gitlab-runner/config.toml # Check runner is registered successfully
   sudo gitlab-runner restart
```

**Windows:**

Check the `config.toml` on `C:\GitLab-Runner`

```bash
   sudo gitlab-runner status
   sudo gitlab-runner restart
```

5. Enable Docker-in-Docker (Optional):

- If your CI/CD pipelines require Docker capabilities, you can enable Docker-in-Docker (DinD) support for the runner. This allows your jobs to run Docker commands within the runner.
  Edit the GitLab Runner configuration file:

```bash
  sudo nano /etc/gitlab-runner/config.toml
```

- Uncomment the line that starts with [[runners]]. IF IT COMMENTED!
- and add/modify the `privileged = true` line underneath it.
- and modify `volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]`
- Save and exit the file.

## Allow Docker in Docker on GitLab Runner

Error:
`error during connect: Post http://docker:2375/v1.40/auth: dial tcp: lookup docker on 192.168.178.1:53: no such host`

This error occurs with docker-based gitlab runners such as the one we’re that are configured using a docker executor. The error message means that the inner docker container doesn’t have a connection to the host docker daemon.

1. config gitlab-runner

`sudo nano /etc/gitlab-runner/config.toml`

In order to fix this, set these options in [runners.docker] in your gitlab-runner config.toml:

```yaml
privileged = true
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```

```yaml
concurrent = 1
check_interval = 0

[session_server]
session_timeout = 1800

[[runners]]
name = "CoreOS-Haar-Runner"
url = "https://gitlab.techoverflow.net/"
token = "bemaiBie8usahMoo9ish"
executor = "docker"
[runners.custom_build_dir]
[runners.cache]
[runners.cache.s3]
[runners.cache.gcs]
[runners.cache.azure]
[runners.docker]
tls_verify = false
image = "docker:stable"
privileged = true
disable_entrypoint_overwrite = false
oom_kill_disable = false
disable_cache = false
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
shm_size = 0
```

### Why this works

Adding "/var/run/docker.sock:/var/run/docker.sock" allows the inner docker container running inside the gitlab-runner to access the outer (host) docker daemon. Without this, it tries to connect to the docker default URL, i.e. <http://docker:2375>. This URL can’t be resolved via DNS (the DNS server in my case is 192.168.178.1, the DNS standard port being 53), hence docker prints the error message listed above.

2. Restart runner

```bash
   sudo gitlab-runner restart
```

[Techoverflow-topic reference](https://techoverflow.net/2021/01/12/how-to-fix-gitlab-ci-error-during-connect-post-http-docker2375-v1-40-auth-dial-tcp-lookup-docker-on-no-such-host/#:~:text=Solution%3A%20This%20error%20occurs%20with%20docker-based%20gitlab%20runners,have%20a%20connection%20to%20the%20host%20docker%20daemon.)

3. Restart runner services

```bash
   sudo gitlab-runner restart
```

```bash
   sudo gitlab-runner status
```

### Config GitLab Runner

`cat /etc/gitlab-runner/config.toml`

## Case i'm administration

Create shared runner for all project

```yaml
http://$IP_ADDRESS/admin/runners
```

[register runner](https://docs.gitlab.com/16.1/ee/user/admin_area/settings/continuous_integration.html#enable-shared-runners-for-new-projects)

(Optional) Update the runner’s `concurrent` value in `C:\GitLab-Runner\config.toml` to allow multiple concurrent jobs as detailed in [advanced configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html) details. Additionally, you can use the advanced configuration details to update your shell executor to use Bash or PowerShell rather than Batch.

> Voila! Runner is installed, running, and will start again after each system reboot. Logs are stored in Windows Event Log.
