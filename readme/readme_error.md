# 1. Docker space full error

```yaml
Running with gitlab-runner 16.1.0 (865283c5) on front-end-app HF5B-BMP, system ID: s_76ea60c10953
Preparing the "docker" executor
00:11
Using Docker executor with image docker:latest ...
Starting service registry.hub.docker.com/library/docker:20.10.16-dind ...
Pulling docker image registry.hub.docker.com/library/docker:20.10.16-dind ...

WARNING: Failed to pull image with policy "always": write /var/lib/docker/tmp/GetImageBlob1461425298: no space left on device (manager.go:237:7s)
ERROR: Job failed: failed to pull image "registry.hub.docker.com/library/docker:20.10.16-dind" with specified policies [always]: write /var/lib/docker/tmp/GetImageBlob1461425298: no space left on device (manager.go:237:7s)
```

## 1. Check Disk Space:

`df -h`

## 2. Clean Up Unused Docker Resources:

```
docker system prune -a --volumes
docker volume prune
```

## 3. Restart Docker Daemon and GitLab Runner:

Restart the Docker daemon and GitLab Runner service to apply any configuration changes made:

```
sudo systemctl restart docker
sudo gitlab-runner restart
```

# 2. Error failed to access localhost/127.0.0.1

To fix this issue you need to configure the `gitlab.rb` configuration at `/etc/gitlab/gitlab.rb`

Pre-requisite

- external_url (http, https) that is not 127.0.0.1

1. Shell to gitlab container

```bash
    docker exec -it gitlab bash
```

2. Vim to `gitlab.rb`

```bash
    vi /etc/gitlab/gitlab.rb
```

3. ADD external_url

```yaml
external_url "EXTERNAL_URL"
such as
external_url "http://192.168.0.1/"
```

4. reconfigure

```bash
    gitlab-ctl reconfigure
```

# 3. SSH is not usable

ssh git@x.x.x.x

can't connect because using docker run publish port that is not 22

_You can check it on gitlab docker container by bash to it_

```bash
    docker exec -it gitlab bash

    # then log the current port of sshd of gitlab container
    cat /var/log/gitlab/sshd/current
```

_Using debug while ssh to it by -vvv flag_

```bash
    ssh -vvv git@gitlab.example.com # or git@x.x.x.x you ip
```

Or Check gitlab config status

```bash
    sudo gitlab-rake gitlab:check
```

But if you following the instruction of in README.md you can shell to you git@x.x.x.x using:

```bash
    ssh -p 2222 -i ~/.ssh/id_rsa git@x.x.x.x
```

<!-- ssh -p 2222 -i ~/.ssh/id_rsa git@192.168.0.9 -->

[ssh-port-no-permission](https://stackoverflow.com/questions/50558100/cant-use-ssh-with-dockerized-gitlab)
[ssh-gitlab-config](https://gitlab.com/gitlab-org/gitlab/-/issues/232770)
